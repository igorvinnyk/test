	// Глобальные переменные
    var proxy = 'https://cors.bridged.cc/';
    
	var currentPage = 2432
	var lastPage = 2432
	var lastActionPageValue = 'current'
	var requestUrl = `${proxy}https://xkcd.com/info.0.json`

        // При Загруке
        window.addEventListener("load",function() {
			// если есть параметр в url генерируем новый url для Запроса
			if(window.location.pathname !== '/') {
				let param = window.location.pathname
				param = param.slice(1);
				requestUrl = `${proxy}https://xkcd.com/${param}/info.0.json`
			}
        	// Обновим ссылки и стили для навигации
        	paginationStyleAndUrl()
        	// Получаем и выводим комикс
        	ajaxGetCommic()
        });

        // Получаем и выводим комикс
        function ajaxGetCommic() {
        	// Запускаем прелоадер
        	loading(true)

        	// Ajax запрос
        	let request = new XMLHttpRequest();
        	request.open('GET',`${requestUrl}`,true);
        	request.addEventListener('readystatechange', function() {
        		// если ответ 200
        		if(request.readyState == 4 && request.status == 200) {
        			response = JSON.parse(request.response)

        			// Выводим полученные параметры в DOM
        			let title = document.getElementById('title-comic')
        			let img = document.getElementById('img-comic')
        			let footerText = document.getElementById('footer-text-img')
        			let footerLink = document.getElementById('footer-text-link')
        			let footerDate = document.getElementById('footer-text-date')
        			// 1. Обновим изображение, тайтл, дату
        			title.innerText = response.title
        			img.setAttribute("alt", response.alt)
        			img.setAttribute("title", response.alt)
        			img.setAttribute("src", response.img)
        			img.setAttribute("src", response.img)
					// 2 Обновим ссылки и дату в футере
					footerText.innerText = response.img
					footerDate.innerText = `${response.day}.${response.month}.${response.year}`
					// footerLink.innerText = window.location.href
 	     			// Выключаем прелоадер 
 	     			loading(false)

        			// Обработка при ответе 404
        		} else if(request.readyState == 4 && request.status == 404) {
        			alert('server error')
        		}
        	});
        	request.send();
        }

        // Функция обработки клика по навигации
        function pagination(value) {
        	event.preventDefault();
        	// если нажата активная кнопка
        	if(lastActionPageValue == value) {
        		return false
        	}
        	// Проверяем какая кнопка нажата и меняем переменную - currentPage - ид комакса
        	if(value == 'first') {
        		if(currentPage == 1) {
        			return false
        		} else {
        			currentPage = 1
        		}
        	}
        	if(value == 'prev') {
        		if(currentPage == 1) {
        			return false
        		} else {
        			currentPage = currentPage -1
        		}
        	}
        	if(value == 'random') {
        		currentPage = Math.floor(Math.random() * (lastPage - 1) + 1)
        	}
        	if(value == 'next') {
        		if(currentPage == lastPage) {
        			return false
        		} else {
        			currentPage = currentPage +1
        		}
        	}
        	if(value == 'last') {
        		if(currentPage == lastPage) {
        			return false
        		} else {
        			currentPage = lastPage
        		}
        	}

        	// Обновим ссылку для Запроса
        	requestUrl = `${proxy}https://xkcd.com/${currentPage}/info.0.json`
        	// Получим комикс по ajax 
        	ajaxGetCommic(currentPage)
        	// Обновим ссылки и стили для навигации
        	paginationStyleAndUrl()
        	// Обновим URL
        	history.pushState(null, null, `/${currentPage}`)
        }

        // Обновим ссылки и стили для навигации
        function paginationStyleAndUrl() {

        	let first = document.getElementsByClassName('pagination-first-button')
        	let prev = document.getElementsByClassName('pagination-prev-button')
        	let next = document.getElementsByClassName('pagination-next-button')
        	let last = document.getElementsByClassName('pagination-last-button')
        	// если первая страница | обновим адрес ссылкок и стили
        	if(currentPage == 1) {
        		for (let element of first) {
        			element.style.opacity = `0.2`
        			element.setAttribute('href', '#')
        		}
        		for (let element of prev) {
        			element.style.opacity = `0.2`
        			element.setAttribute('href', '#')
        		}
        		for (let element of next) {
        			element.style.opacity = `1`
        			element.setAttribute('href', currentPage +1)
        		}
        		for (let element of last) {
        			element.style.opacity = `1`
        			element.setAttribute('href', lastPage)
        		}

        	// если последняя страница | обновим адрес ссылкок и стили
        } else if(currentPage == lastPage) {
        	// обновим стили
        	for (let element of first) {
        		element.style.opacity = `1`
        		element.setAttribute('href', 1)
        	}
        	for (let element of prev) {
        		element.style.opacity = `1`
        		element.setAttribute('href', currentPage -1)
        	}
        	for (let element of next) {
        		element.style.opacity = `0.2`
        		element.setAttribute('href', '#')
        	}
        	for (let element of last) {
        		element.style.opacity = `0.2`
        		element.setAttribute('href', '#')
        	}

        	// если не последняя и не первая страница | обновим адрес ссылкок и стили
        } else {

        	for (let element of first) {
        		element.style.opacity = `1`
        		element.setAttribute('href', 1)
        	}
        	for (let element of prev) {
        		element.style.opacity = `1`
        		element.setAttribute('href', currentPage -1)
        	}
        	for (let element of next) {
        		element.style.opacity = `1`
        		element.setAttribute('href', currentPage +1)
        	}
        	for (let element of last) {
        		element.style.opacity = `1`
        		element.setAttribute('href', lastPage)
        	}
        }
    }

        // Переключатель прелоадера
        function loading(value = false) {
        	if(value)  {
        		document.getElementById('spinner').style.display = 'block'
        		document.getElementById('content').style.display = 'none'
        	}
        	if(!value)  {
        		document.getElementById('spinner').style.display = 'none'
        		document.getElementById('content').style.display = 'block'
        	}
        }